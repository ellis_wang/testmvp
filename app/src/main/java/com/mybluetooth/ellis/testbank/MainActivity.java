package com.mybluetooth.ellis.testbank;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

public class MainActivity extends AppCompatActivity implements MainView {
    @BindView(R.id.user_id)
    EditText userId;
    @BindView(R.id.user_pwd)
    EditText userPwd;
    @BindView(R.id.input_pwd_edit)
    TextInputLayout pwdTextInputLayout;
    @BindView(R.id.input_user_edit)
    TextInputLayout userTextInputLayout;
    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mainPresenter = new MainPresenter(this);

    }

    @OnClick(R.id.login_button)
    public void loginBtn() {
//        mauth = FirebaseAuth.getInstance();
//        if (!userId.getText().toString().isEmpty() & !userPwd.getText().toString().isEmpty()) {
//            mauth.signInWithEmailAndPassword(userId.getText().toString(), userPwd.getText().toString())
//                    .addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception e) {
//                            Log.w("OnFailure", e.getMessage());
//                            Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
//
//                        }
//                    })
//                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                        @Override
//                        public void onComplete(@NonNull Task<AuthResult> task) {
//                            if (task.isSuccessful()) {
//                                Log.w("Complete", task.getResult().getUser().getEmail());
//
//                            }
//                        }
//                    });
//        }
//        if (userId.getText().toString().isEmpty()) {
//            userTextInputLayout.setError("Mail 不得為空");
//        }
//        if (userPwd.getText().toString().isEmpty()) {
//            pwdTextInputLayout.setError("密碼 不得為空");
//        }
        mainPresenter.loginOnClick(userId.getText().toString(), userPwd.getText().toString());

    }

    @OnTouch({R.id.user_id, R.id.user_pwd})
    public boolean onTouuch(View view, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                userTextInputLayout.setError("");
                pwdTextInputLayout.setError("");
                break;
            default:
                return false;
        }
        return false;
    }

    @Override
    public void userTextInputEmpty() {
        userTextInputLayout.setError("Mail 不得為空");
    }

    @Override
    public void pwdTextInputEmpty() {
        pwdTextInputLayout.setError("密碼 不得為空");
    }

    @Override
    public void toastMsg(String meg) {
        Toast.makeText(this, meg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginSuccess() {
        Log.w("loginSuccess", "dddddd");
    }
}
