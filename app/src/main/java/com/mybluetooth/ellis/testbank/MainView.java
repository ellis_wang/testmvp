package com.mybluetooth.ellis.testbank;

public interface MainView {
    void userTextInputEmpty();
    void pwdTextInputEmpty();
    void toastMsg(String meg);
    void loginSuccess();
}
