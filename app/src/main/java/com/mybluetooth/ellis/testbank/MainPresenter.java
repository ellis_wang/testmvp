package com.mybluetooth.ellis.testbank;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainPresenter {
    private MainView mainView;
//    private MainModel mainModel;

    public MainPresenter(MainView mView) {
        this.mainView = mView;
//        mainModel = new MainModel();
    }

    public void loginOnClick(String userid, String userpwd) {
        Log.d("P", "onloginclick");
        if (isStrEmpty(userid)) {
            mainView.userTextInputEmpty();
        }
        if (isStrEmpty(userpwd)) {
            mainView.pwdTextInputEmpty();
        }

//        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
//        firebaseAuth.signInWithEmailAndPassword(userid, userpwd)
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.w("OnFailure", e.getMessage());
//                        mainView.toastMsg(e.getMessage());
//                    }
//                })
//                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (task.isSuccessful()) {
//                            Log.w("Complete", task.getResult().getUser().getEmail());
//                            mainView.loginSuccess();
//                        }
//                    }
//                });
        if (!isStrEmpty(userid) && !isStrEmpty(userpwd)) {
            new authLogin().execute(userid, userpwd);

        }
    }

    private class authLogin extends AsyncTask<String, Boolean, Boolean> {
        private Boolean isFail;
        private String msg;

        @Override
        protected Boolean doInBackground(String... strings) {
            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            firebaseAuth.signInWithEmailAndPassword(strings[0], strings[1])
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("OnFailure", e.getMessage());
                            msg = e.getMessage();
                            isFail = true;
                        }
                    })
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.w("Complete", task.getResult().getUser().getEmail());
                                isFail = false;
                            }
                        }
                    });
            while (true) {
                if (isFail != null) {
                    break;
                }
            }
            return isFail;
        }

        @Override
        protected void onPostExecute(Boolean isFail) {
            super.onPostExecute(isFail);
            if (isFail) {
                mainView.toastMsg(msg);
            } else {
                mainView.loginSuccess();
            }
        }
    }
    public boolean isStrEmpty(String str) {
        if (TextUtils.isEmpty(str)) {
            return true;
        } else return false;
    }

}
